# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: hmouhib <hmouhib@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2024/02/27 14:30:59 by hmouhib           #+#    #+#              #
#    Updated: 2024/02/27 16:52:25 by hmouhib          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# --- Variables ---- #
NAME				=	minitalk
INCDIR				=	include
LIBDIR				=	lib
SRCDIR				=	src
SERVER				=	server
CLIENT				=	client
SERVER_BONUS		=	server_bonus
CLIENT_BONUS		=	client_bonus
LIBFT				=	libft
FT_PRINTF			=	ft_printf

SRCS_SERVER			=	$(SRCDIR)/server.c
SRCS_CLIENT			=	$(SRCDIR)/client.c
SRCS_SERVER_BONUS	=	$(SRCDIR)/server_bonus.c
SRCS_CLIENT_BONUS	=	$(SRCDIR)/client_bonus.c

LIBS				=	$(LIBDIR)/$(LIBFT)			\
						$(LIBDIR)/$(FT_PRINTF)

LIB_LIBFT			=	$(LIBDIR)/$(LIBFT)/$(LIBFT).a
LIB_FT_PRINTF		=	$(LIBDIR)/$(FT_PRINTF)/libftprintf.a

# ----- ~Alias ----- #
CC		=	cc
CFLAGS	=	-Wall -Wextra -Werror
RM		=	rm -rf

all: banner banner_mandatory $(NAME)

banner:
	@echo "\033[1;37m        __  __ _       _ _______    _ _      \033[0m"
	@echo "\033[1;37m       |  \/  (_)     (_)__   __|  | | |     \033[0m"
	@echo "\033[1;37m       | \  / |_ _ __  _   | | __ _| | | __  \033[0m"
	@echo "\033[1;37m       | |\/| | | '_ \| |  | |/ _\` | | |/ / \033[0m"
	@echo "\033[1;37m       | |  | | | | | | |  | | (_| | |   <   \033[0m"
	@echo "\033[1;37m       |_|  |_|_|_| |_|_|  |_|\__,_|_|_|\_\  \n\033[0m"

banner_mandatory:
	@echo "\033[1;35m                                            MANDATORY\n  \033[0m"

banner_bonus:
	@echo "\033[1;35m                                            BONUS\n  \033[0m"

$(NAME): $(SERVER) $(CLIENT)

bonus: banner banner_bonus $(SERVER_BONUS) $(CLIENT_BONUS)

# -- ~Compilation -- #
$(SERVER): $(SRCS_SERVER) | mklibs
	@echo '   🔨 building server ...'
	@$(CC) $(CFLAGS) $(LIB_LIBFT) $(LIB_FT_PRINTF) $^ -I $(INCDIR) -o $@
	@echo '   ✨ done !'

$(CLIENT): $(SRCS_CLIENT) | mklibs
	@echo '   🔨 building client ...'
	@$(CC) $(CFLAGS) $(LIB_LIBFT) $(LIB_FT_PRINTF) $^ -I $(INCDIR) -o $@
	@echo '   ✨ done !'

$(SERVER_BONUS): $(SRCS_SERVER_BONUS) | mklibs
	@echo '   🔨 building server ...'
	@$(CC) $(CFLAGS) $(LIB_LIBFT) $(LIB_FT_PRINTF) $^ -I $(INCDIR) -o $@
	@echo '   ✨ done !'

$(CLIENT_BONUS): $(SRCS_CLIENT_BONUS) | mklibs
	@echo '   🔨 building client ...'
	@$(CC) $(CFLAGS) $(LIB_LIBFT) $(LIB_FT_PRINTF) $^ -I $(INCDIR) -o $@
	@echo '   ✨ done !'
	
# ---- Commands ---- #
mklibs:
	@make -s -C $(LIBDIR)/$(LIBFT)
	@make -s -C $(LIBDIR)/$(FT_PRINTF)
	@echo '   ✨ required libs are ready to use ✅!'

clean:
	@make -s -C $(LIBDIR)/$(LIBFT) clean
	@make -s -C $(LIBDIR)/$(FT_PRINTF) clean
	@echo '   ✨ required libs are cleaned 🧹!'

fclean: clean
	@$(RM) $(SERVER) $(CLIENT)
	@$(RM) $(SERVER_BONUS) $(CLIENT_BONUS)
	@make -s -C $(LIBDIR)/$(LIBFT) fclean
	@make -s -C $(LIBDIR)/$(FT_PRINTF) fclean

re: fclean all

.PHONY: all bonus clean fclean mklibs re banner
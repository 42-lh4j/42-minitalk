/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hmouhib <hmouhib@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/26 16:38:32 by hmouhib           #+#    #+#             */
/*   Updated: 2024/03/02 15:35:25 by hmouhib          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

#include "libftprintf.h"
#include "ansi_colors.h"
#include "libft.h"

static int	check_pid(const char *pid)
{
	int	pid_;

	while (*pid)
	{
		if (!(*pid >= '0' && *pid <= '9'))
		{
			ft_printf("Error: Invalid pid!\n");
			exit(1);
		}
		pid++;
	}
	pid_ = ft_atoi(pid);
	if (pid_ <= 0)
	{
		ft_printf("Error: Invalid pid!\n");
		exit(1);
	}
	return (pid_);
}

/** @brief	a simple help menu for this program ! */
static void	client_help(const char *pname)
{
	ft_printf("\n\t\t---------- %sMiniTalk Client%s ----------\n\n",
		BWHT, CRESET);
	ft_printf("\t%s<DESCRIPTION>%s\n\n", BWHT, CRESET);
	ft_printf("\t\t\tthis is a small data sender program that sends\n");
	ft_printf("\t\t\ta message using %sUNIX signals%s to another server\n",
		UMAG, CRESET);
	ft_printf("\t\t\tprogram that act's as a receiver and print's\n");
	ft_printf("\t\t\tout the message to the screen ... !\n\n");
	ft_printf("\t\t\thope you like it ^w^ !\n\n");
	ft_printf("\t%s<USAGE>%s\n\n", BWHT, CRESET);
	ft_printf("\t\t\t%s <pid> <message to send ...>\n\n", pname);
}

/**
 *	@brief			this function send's a <byte> *bit* by *bit* to
 * 					the server using it's process id <pid> ... !
 * 	@param			byte the byte to send !
 * 	@param			pid the server's process id !
 */
static void	send_bits(char byte, pid_t pid)
{
	int	bits_sent;

	bits_sent = 0;
	while (bits_sent < 8)
	{
		if (byte & 0b10000000)
		{
			if (kill(pid, SIGUSR1))
			{
				ft_printf("Error: Invalid pid !\n");
				exit(1);
			}
		}
		else
		{
			if (kill(pid, SIGUSR2))
			{
				ft_printf("Error: Invalid pid !\n");
				exit(1);
			}
		}
		usleep(255);
		byte = byte << 1;
		bits_sent++;
	}
}

/** @note	i wanna send the null byte too
 *			so the server knows that we're
 * 			done sending the message ...
 */
int	main(int argc, const char **argv)
{
	size_t			i;
	pid_t			pid_;
	const char		*message;

	if (argc == 3)
	{
		i = 0;
		pid_ = check_pid(argv[1]);
		message = argv[2];
		while (i < ft_strlen(message) + 1)
		{
			send_bits(message[i++], pid_);
			usleep(42);
		}
	}
	else
		client_help(argv[0]);
	write(1, "\n", 1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_bonus.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hmouhib <hmouhib@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/26 18:23:51 by hmouhib           #+#    #+#             */
/*   Updated: 2024/03/02 15:30:37 by hmouhib          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

#include "libftprintf.h"
#include "ansi_colors.h"

static void	put_banner(void)
{
	int			i;
	const char	*banner[] = {
		"\t\t __  __ _       _ _______    _ _    \n",
		"\t\t|  \\/  (_)     (_)__   __|  | | |   \n",
		"\t\t| \\  / |_ _ __  _   | | __ _| | | __\n",
		"\t\t| |\\/| | | '_ \\| |  | |/ _` | | |/ /\n",
		"\t\t| |  | | | | | | |  | | (_| | |   < \n",
		"\t\t|_|  |_|_|_| |_|_|  |_|\\__,_|_|_|\\_\\\n",
		NULL
	};

	i = 0;
	while (banner[i])
		ft_printf("%s%s%s", BWHT, banner[i++], CRESET);
}

static void	put_received_byte(unsigned char *byte, int *counter, int *start)
{
	if (*byte == 0)
	{
		ft_printf("\n");
		*start = 1;
	}
	else
		ft_printf("%c", *byte);
	*counter = 0;
	*byte = 0;
}

static void	handler(int signal, siginfo_t *info, void *ctx)
{
	static char	byte = 0;
	static int	bits_counter = 0;
	static int	start = 1;

	(void)ctx;
	if (start)
		ft_printf(" %sreceived%s %s->%s ", BGRN, CRESET, BWHT, CRESET);
	start = 0;
	if (signal == SIGUSR1)
	{
		byte |= (1 << (7 - bits_counter));
		kill(info->si_pid, SIGUSR1);
	}
	if (signal == SIGUSR2)
	{
		byte |= (0 << (7 - bits_counter));
		kill(info->si_pid, SIGUSR2);
	}
	bits_counter++;
	if (bits_counter == 8)
		put_received_byte(&byte, &bits_counter, &start);
}

int	main(void)
{
	struct sigaction		sa;

	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = &handler;
	sigaction(SIGUSR1, &sa, NULL);
	sigaction(SIGUSR2, &sa, NULL);
	ft_printf("\n");
	put_banner();
	ft_printf("\n\t\t\t\t\t PID -> %d\n\n", getpid());
	while (1)
		pause();
}
